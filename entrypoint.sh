#!/bin/sh
set -eu

if [ -z "${1:-}" ]; then
    exec /sbin/tini -- unbound_exporter -unbound.ca "${UNBOUND_CA}" -unbound.cert "${UNBOUND_CERT}" -unbound.host "${UNBOUND_HOST}" -unbound.key "${UNBOUND_KEY}"
fi

exec /sbin/tini -- "$@"
