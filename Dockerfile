# Build
ARG GOLANG_VERSION=1.19
FROM golang:${GOLANG_VERSION}-alpine as builder

ARG EXPORTER_VERSION=v0.4.1

# hadolint ignore=DL3018
RUN apk add --no-cache git && \
    git clone --depth 1 --branch ${EXPORTER_VERSION} https://github.com/letsencrypt/unbound_exporter.git /workspace

WORKDIR /workspace

RUN go build -o unbound_exporter .


# Certs
# Need Unbound certs until this issue is solved:
# https://github.com/letsencrypt/unbound_exporter/issues/44
FROM alpine:3 as cert
# hadolint ignore=DL3018
RUN apk add --no-cache --update unbound openssl && \
    unbound-control-setup


# Main
FROM alpine:3

LABEL maintainer="Radek Sprta <mail@radeksprta.eu>"
LABEL org.opencontainers.image.authors="Radek Sprta <mail@radeksprta.eu>"
LABEL org.opencontainers.image.description="Prometheus metrics exporter for the Unbound DNS resolver."
LABEL org.opencontainers.image.documentation="https://gitlab.com/radek-sprta/docker-unbound_exporter/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="GNU General Public License v3"
LABEL org.opencontainers.image.source="https://gitlab.com/radek-sprta/docker-unbound_exporter"
LABEL org.opencontainers.image.title="rsprta/unbound_exporter"
LABEL org.opencontainers.image.url="https://gitlab.com/radek-sprta/docker-unbound_exporter"

ARG TINI_VERSION=0.19.0-r0

EXPOSE 9167

HEALTHCHECK --interval=10s --timeout=5s --retries=3 --start-period=10s \
    CMD curl --fail 127.0.0.1:9167/metrics || exit 1

ENV UNBOUND_CA=/etc/unbound/unbound_server.pem \
    UNBOUND_CERT=/etc/unbound/unbound_control.pem \
    UNBOUND_HOST=tcp://localhost:8953 \
    UNBOUND_KEY=/etc/unbound/unbound_control.key

RUN apk add --no-cache --upgrade tini="${TINI_VERSION}" && \
    mkdir /etc/unbound

COPY --from=builder /workspace/unbound_exporter /usr/local/bin
COPY --from=cert /etc/unbound/unbound_control.key /etc/unbound
COPY --from=cert /etc/unbound/unbound_control.pem /etc/unbound
COPY --from=cert /etc/unbound/unbound_server.pem /etc/unbound
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
