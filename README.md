# rsprta/unbound_exporter [![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/rsprta/unbound_exporter)](https://hub.docker.com/r/rsprta/unbound_exporter) [![Docker Pulls](https://img.shields.io/docker/pulls/rsprta/unbound_exporter)](https://hub.docker.com/r/rsprta/unbound_exporter) [![Pipeline status](https://gitlab.com/radek-sprta/docker-unbound_exporter/badges/master/pipeline.svg)](https://gitlab.com/radek-sprta/docker-unbound_exporter/commits/master)

## Quick reference

- **Maintained by**: [Radek Sprta](https://gitlab.com/radek-sprta)
- **Where to get help**: [Repository Issues](https://gitlab.com/radek-sprta/docker-unbound_exporter/-/issues)

## Description

Prometheus metrics exporter for the Unbound DNS resolver.

## Usage

The simplest way to run the container is the following command:

```bash
docker run --detach --port 9167:9167 rsprta/unbound_exporter
```

Or using `docker-compose.yml`:

```yaml
version: "3"
services:
  unbound_exporter:
    container_name: unbound_exporter
    image: rsprta/unbound_exporter
    ports:
      - "9167:9167"
    restart: unless-stopped
```

### Environment variables

| Variable | Description | Type | Default value |
| -------- | ----------- | ---- | ------------- |
| **UNBOUND_CA** | Path to CA cert | *optional* | /etc/unbound/unbound_server.pem
| **UNBOUND_CERT** | Path to cert | *optional* | /etc/unbound/unbound_control.pem 
| **UNBOUND_HOST** | Host to scrape | *optional* | tcp://localhost:8953
| **UNBOUND_KEY** | Path to cert key | *optional* | /etc/unbound/unbound_control.key

## Contact

- [mail@radeksprta.eu](mailto:mail@radeksprta.eu)
- [incoming+radek-sprta/docker-unbound_exporter@gitlab.com](incoming+radek-sprta/docker-unbound_exporter@gitlab.com)

## License

GNU General Public License v3

## Credits

This package was created with [Cookiecutter][cookiecutter] from [cookiecutter-docker-multiarch](https://gitlab.com/radek-sprta/cookiecutter-docker-multiarch).

[cookiecutter]: https://github.com/audreyr/cookiecutter
